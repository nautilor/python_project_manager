# Python Project manager

This is a simple tools to manage python projects


## Commands

    # CREATE A NEW PROJECT
    ppm -n my_project
    ppm --new my_project

    # ADD A DEPENDENCY TO THE PROJECT
    ppm -a dependency
    ppm --add dependency

    # INSTALL ALL DEPENDENCY OF A PROJECT
    ppm -i
    ppm --install

    # RUN THE PROGRAM
    ppm -r
    ppm --run


## Environment variables

When doing `ppm -r/--run` you can have a `.env.json` in you project with all the environment vaiables you need

    + my_project
    |
    |---+
        |--+ root
        |  |--+ __main__.py
        |
        |--+ requirements.py
        |
        |--+ .env.json

example of `.env.json`

    {
        "foo": "bar",
        "bar": "foo"        
    }
    